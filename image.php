<?php
	header("Content-type: image/png");
	session_start();
	
	$imgWidth=($_GET['width'])?$_GET['width']:500;
	$imgHeight=($_GET['height'])?$_GET['height']:200;
	$circleRadius = ($_GET['radius'])?$_GET['radius']:20;
	
	
	/* generate image */
	$image=imagecreate($imgWidth, $imgHeight);
	$color = imagecolorallocate($image, 255, 255, 255);
	imagefill ( $image , 0 , 0 , $color );
	/* set image coords and colore */
	$x= rand($imgWidth/2-($imgWidth*0.3),$imgWidth/2+($imgWidth*0.3));
	$y= rand( $imgHeight/2-($imgHeight*0.3), $imgHeight/2+($imgHeight*0.3));	
	$col = imagecolorallocate($image, rand(0,255), rand(0,255), rand(0,255));
	$radius = rand($circleRadius-$circleRadius*0.3, $circleRadius+$circleRadius*0.3);
	
	//echo "X: $x<br>Y: $y<br>";
	
	$_SESSION['coordx'] = $x;
	$_SESSION['coordy'] = $y;
	$_SESSION['radius'] = $radius;
	
	
	

    dashedcircle($image, $x, $y, $radius, $col, $color, 10);
    

	for( $n=0; $n < 8 ;$n++ )
	{
		$x= mt_rand($imgWidth/2-($imgWidth*0.3),$imgWidth/2+($imgWidth*0.3));
		$y= mt_rand( $imgHeight/2-($imgHeight*0.3), $imgHeight/2+($imgHeight*0.3));	
		$col = imagecolorallocate($image, rand(0,255), rand(0,255), rand(0,255));
		$radius = mt_rand($circleRadius-$circleRadius*0.3, $circleRadius-$circleRadius*0.3);
		imageellipse ( $image , $x , $y , $circleRadius,$circleRadius , $col );
	}
		

		
		
	imagepng($image);
    imagedestroy($image);


function dashedcircle($image, $cx, $cy, $radius, $fg, $bg, $dashes = 5) { 

    $cnt = 360/($dashes-1)/2;

    $style = array();
    for($n = 0; $n< 2; $n++) {
        for($i = 0; $i< $cnt; $i++) {
            $style[] = $n == 1 ? $fg : $bg;
        }
    }

    imagesetstyle ($image, $style);

    imagearc($image,$cx,$cy,$radius,$radius,0,360,IMG_COLOR_STYLED);

} 
    

?>
